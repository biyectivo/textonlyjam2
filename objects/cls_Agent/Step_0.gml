/// @description FSM execution
if (!Game.paused && initialized) {
	previous_state = state;

	// Execute current state
	if (Game.debug) {
		show_debug_message("["+Game.game_title+"] "+string(event_type)+" "+name+": "+state_name+" / "+string(image_index));
	}

	if (script_exists(asset_get_index("fnc_"+name+"FSM_"+state_name))) {
		script_execute(state);	
	}
}
