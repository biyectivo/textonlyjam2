/// @description Initialize agent

event_inherited(); // Initialize true

// State
state = asset_get_index("fnc_"+name+"FSM_Idle");
state_name = "Idle";
last_state = state;
last_state_name = state_name;
previous_state = state;
facing = FACING.EAST;
animation_name = state_name;

// Animation
animation_frame_count = 0; // the ith frame of the animation
animation_frame = 0; // the actual frame displayed on the ith frame of animation
reset_animation = false;
script_execute(asset_get_index("fnc_"+name+"FSM_Animate"), true);
alarm[0] = animation_speeds[? animation_name];

// Reset input
player_horizontal_input = 0;
player_vertical_input = 0;
	