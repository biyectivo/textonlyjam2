/// @description Projectile step event
if (!Game.paused && initialized) {	
	x = x + projectile_data.move_speed * cos(projectile_angle);
	y = y - projectile_data.move_speed * sin(projectile_angle);
	
	var _faction = creator_unit_id.opposing_type;
	var _current_hit_list = ds_list_create();
	
	var _n = instance_place_list(x, y, obj_Unit, _current_hit_list, true);			
	
	for (var _i=0; _i<_n; _i++) {
		// If unit is of opposing faction and has not been hit, hit it, reduce projectile hp and add it to the hit list
		var _id = _current_hit_list[|_i];
		if (_id.unit_data.type == _faction && ds_list_find_index(projectile_units_hit, _id) == -1 && projectile_hp > 0) {
			ds_list_add(projectile_units_hit, _id);
			_id.hp = _id.hp - projectile_data.attack_damage;
			_id.alarm[4] = HIT_SHADER_EFFECT_DURATION;
			projectile_hp--;
		}
	}
	
	ds_list_destroy(_current_hit_list);
}