/// @description Update based on the projectile ID
projectile_data = Game.projectiles[? projectile_id];
projectile_hp = projectile_data.pierce_num + 1; // 0 pierce = 1 health, etc.
projectile_units_hit = ds_list_create();
alarm[0] = projectile_data.animation_length;

initialized = true;