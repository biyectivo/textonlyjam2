/// @description Projectile draw event
if (!Game.paused && initialized) {
	//if live_call() return live_result;
	draw_set_color(c_gray);
	draw_set_halign(fa_center);
	draw_set_valign(fa_bottom);
	draw_set_font(fnt_Particle_Projectile);
	
	draw_text_transformed(x, y, "o", 0.5, 0.5, 0);
	
}