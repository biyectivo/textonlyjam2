/// @description Projectile end draw event - check hp
if (!Game.paused && initialized) {
	if (projectile_hp <= 0 || x < -100 || x > room_width + 100 || y < -100 || y > room_height + 100) {
		instance_destroy();
	}
}