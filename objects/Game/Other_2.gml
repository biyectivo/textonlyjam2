// Mouse cursor
window_set_cursor(cr_none);
cursor_sprite = spr_Cursor;

// Load unit data
unit = function(_param_list) constructor {
	unit_numid = fnc_Real(_param_list[|0]);
	name = _param_list[|1];
	purpose = _param_list[|2];
	description = _param_list[|3];
	type = _param_list[|4];
	energy_cost = fnc_Real(_param_list[|5]);
	move_speed = fnc_Real(_param_list[|6]);
	requires_numid = fnc_Real(_param_list[|7]);
	max_hp = fnc_Real(_param_list[|8]);
	energy_creation_rate = fnc_Real(_param_list[|9]);
	energy_creation_num = fnc_Real(_param_list[|10]);
	attack_rate = fnc_Real(_param_list[|11]);
	attack_range = fnc_Real(_param_list[|12]);
	attack_damage_hp = fnc_Real(_param_list[|13]);
	attack_projectile_id = _param_list[|14];
	attack_projectile_num = fnc_Real(_param_list[|15]);
	attack_aoe_radius = fnc_Real(_param_list[|16]);
	attack_type = _param_list[|17];
	heal_rate = fnc_Real(_param_list[|18]);
	heal_hp = fnc_Real(_param_list[|19]);
	heal_aoe_radius = fnc_Real(_param_list[|20]);
	summon_rate = fnc_Real(_param_list[|21]);
	summon_numid_list = _param_list[|22];
	special_script = _param_list[|23];
	special_params = _param_list[|24];
	idle_animation_length = fnc_Real(_param_list[|25]);
	move_animation_length = fnc_Real(_param_list[|26]);
	attack_animation_length = fnc_Real(_param_list[|27]);
	base_letter = _param_list[|28];
	base_color = _param_list[|29];
	base_xscale = fnc_Real(_param_list[|30]);
	base_adornment1 = _param_list[|31];
	base_adornment1_color = _param_list[|32];
	base_adornment1_xoffset = fnc_Real(_param_list[|33]);
	base_adornment1_yoffset = fnc_Real(_param_list[|34]);
	base_adornment1_xscale = fnc_Real(_param_list[|35]);
	base_adornment2 = _param_list[|36];
	base_adornment2_color = _param_list[|37];
	base_adornment2_xoffset = fnc_Real(_param_list[|38]);
	base_adornment2_yoffset = fnc_Real(_param_list[|39]);
	base_adornment2_xscale = fnc_Real(_param_list[|40]);
	action_letter = _param_list[|41];
	action_color = _param_list[|42];
	action_xscale = fnc_Real(_param_list[|43]);
	action_adornment1 = _param_list[|44];
	action_adornment1_color = _param_list[|45];
	action_adornment1_xoffset = fnc_Real(_param_list[|46]);
	action_adornment1_yoffset = fnc_Real(_param_list[|47]);
	action_adornment1_xscale = fnc_Real(_param_list[|48]);
	action_adornment2 = _param_list[|49];
	action_adornment2_color = _param_list[|50];
	action_adornment2_xoffset = fnc_Real(_param_list[|51]);
	action_adornment2_yoffset = fnc_Real(_param_list[|52]);
	action_adornment2_xscale = fnc_Real(_param_list[|53]);
}

Game.units = ds_map_create();
var _fid = file_text_open_read("unit.txt");
while (!file_text_eof(_fid)) {
	var _str = file_text_readln(_fid);
	var _str_list = fnc_StringToList(_str, "|");
	Game.units[? _str_list[| 1]] = new unit(_str_list);
}

file_text_close(_fid);

// Load projectile data
projectile = function(_param_list) constructor {
	projectile_numid = fnc_Real(_param_list[|0]);
	name = _param_list[|1];
	move_speed = fnc_Real(_param_list[|2]);
	animation_length = fnc_Real(_param_list[|3]);
	attack_damage = fnc_Real(_param_list[|4]);
	attack_type = _param_list[|5];
	pierce_num = fnc_Real(_param_list[|6]);
	end_behavior = _param_list[|7];
}




Game.projectiles = ds_map_create();
var _fid = file_text_open_read("projectile.txt");
while (!file_text_eof(_fid)) {
	var _str = file_text_readln(_fid);
	var _str_list = fnc_StringToList(_str, "|");
	Game.projectiles[? _str_list[| 1]] = new projectile(_str_list);
}

file_text_close(_fid);



// Go to next room
//room_goto(room_UI_Title);
room_goto(room_Game_1);
