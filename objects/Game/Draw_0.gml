/// @description 
//if live_call() return live_result;
if (!Game.paused) {
	//if live_call() return live_result;
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _rect_width = _w/NUM_COLS;
	var _rect_height = _h/NUM_ROWS;
	var _y_offset = 30;
	
	draw_set_font(fnt_Terrain);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
		
	for (var _row = 1; _row < NUM_ROWS-1; _row++) {
		for (var _col = 1; _col < NUM_COLS-1; _col++) {
			if ((_row+_col) % 2 == 0) {
				var _color = $339C21; //$387F27;
			}
			else {
				var _color = $00FF00; //$42992D;
			}
			draw_set_color(_color);
			
			for (var _i=0; _i<8; _i++) {
				draw_text(_col*_rect_width, _row*_rect_height+_i*8+_y_offset, string_repeat(".",11));
			}
		}
	}
}