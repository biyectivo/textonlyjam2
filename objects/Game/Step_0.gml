if (!Game.lost && primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_start)) {		
	event_perform(ev_keypress, vk_escape);
}

if (Game.lost && Game.paused && ((primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_face1)) || (keyboard_check_pressed(vk_enter)))) {
	room_restart();
}

// Hide collision layer	
if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}

// Room-specific code
if (!Game.paused) {
	if (room == room_Game_1) {
		current_step++;
		
		if (keyboard_check_pressed(vk_enter)) {		
			battlefield[0][0] = Game.units[? "Shooter"].unit_numid;
			var _coords = fnc_Coords(0,0);
			var _id = instance_create_layer(_coords[0], _coords[1], layer_get_id("lyr_Instances"), obj_Unit);
			with (_id) {
				unit_id = "Shooter";
				event_perform(ev_other, ev_user0);
			}			
		}
		
		if (keyboard_check_pressed(vk_space)) {
			with (obj_Unit) {
				show_debug_message(string(id)+" "+string(unit_data.name)+": at state "+string(state)+" > "+string(nearest_opposing_unit_array)+" "+string(nearest_opposing_unit_array[0] != noone ? nearest_opposing_unit_array[0].hp : noone));
			}
		}
		
		if (keyboard_check_pressed(vk_backspace)) {
			for (var _row = 0; _row < NUM_ROWS-2; _row++) {
				var _txt = "";
				for (var _col =0; _col < NUM_COLS-2; _col++) {
					_txt = _txt + string(battlefield[_row][_col]);	
				}
				show_debug_message(_txt);
			}
		}
		
	}
}
