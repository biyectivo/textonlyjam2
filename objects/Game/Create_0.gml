//*****************************************************************************
// Global variable declarations
//*****************************************************************************

debug = false;
//username = "Player";

//*****************************************************************************
// Randomize
//*****************************************************************************

randomize();

//*****************************************************************************
// Set camera target
//*****************************************************************************
/*
camera_target = obj_Player;
camera_shake = false;
camera_smoothness = 0.08;
*/
//*****************************************************************************
// Menu item definitions
//*****************************************************************************

menu_items = array_create(4);
menu_items[0] = "Start game";
menu_items[1] = "Help";
menu_items[2] = "Options";
menu_items[3] = "Credits";
menu_items[4] = "Quit";

option_items = array_create(4);
option_items[0] = "Music";
option_items[1] = "Sounds";
option_items[2] = "Volume";
option_items[3] = "Fullscreen";
option_items[4] = "Controls";
option_items[5] = "Name";

control_indices = [];
controls = ds_map_create();
control_names = ds_map_create();

control_indices[0] = "left";
control_indices[1] = "right";
control_indices[2] = "up";
control_indices[3] = "down";

controls[? "left"] = vk_left;
controls[? "right"] = vk_right;
controls[? "up"] = vk_up;
controls[? "down"] = vk_down;


control_names[? "left"] = "Move Left";
control_names[? "right"] = "Move Right";
control_names[? "up"] = "Move Up";
control_names[? "down"] = "Move Down";

name_being_modified = false;

wait_for_input = false;
key_being_remapped = noone;

option_type = ds_map_create();
ds_map_add(option_type, option_items[0], "toggle");
ds_map_add(option_type, option_items[1], "toggle");
ds_map_add(option_type, option_items[2], "slider");
ds_map_add(option_type, option_items[3], "toggle");
ds_map_add(option_type, option_items[4], "");
ds_map_add(option_type, option_items[5], "input");

option_value = ds_map_create();
ds_map_add(option_value, option_items[0], true);
ds_map_add(option_value, option_items[1], true);
ds_map_add(option_value, option_items[2], 1.0);
ds_map_add(option_value, option_items[3], false);
ds_map_add(option_value, option_items[4], noone);
ds_map_add(option_value, option_items[5], "Player");

credits[0] = "[fnt_Menu][fa_middle][fa_center][c_white]2021[c_yellow]Programming: [c_white]biyectivo";
credits[1] = "[fa_middle][fa_center][c_white][c_white](Jose Alberto Bonilla Vera)";

game_title = "Game1";
scoreboard_game_id = "Game1";

primary_gamepad = -1;
start_drag_drop = false;

// Particle system and emitter variables - define to avoid not set
particle_system = noone;
particle_emitter_fire = noone;

pause_screenshot = noone;

//*****************************************************************************
// Data structures
//*****************************************************************************
grid = noone;

fnc_InitializeGameStartVariables();
