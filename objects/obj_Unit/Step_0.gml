/// @description Unit step event: state FSM
if (!Game.paused && initialized) {
	previous_state = state;

	// Execute current state
	if (Game.debug) {
		show_debug_message("["+Game.game_title+"] "+string(event_type)+" "+string(id)+" "+string(unit_id)+": "+state);
	}

	// Execute state
	if (script_exists(asset_get_index("fnc_StateFSM_"+string_replace(unit_id, " ", "")+"_"+state))) {
		script_execute(asset_get_index("fnc_StateFSM_"+string_replace(unit_id, " ", "")+"_"+state));
	}
	
}
