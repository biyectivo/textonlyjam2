/// @description Unit draw event: animate FSM
if (!Game.paused && initialized) {	

	// Execute state animation
	if (script_exists(asset_get_index("fnc_AnimateFSM_"+string_replace(unit_id, " ", "")+"_"+state))) {
		script_execute(asset_get_index("fnc_AnimateFSM_"+string_replace(unit_id, " ", "")+"_"+state));
	}
	
	if (Game.debug) {
		draw_set_alpha(0.2);
		draw_set_color(c_red);
		draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, false);
		draw_set_alpha(1);
	}
}

