/// @description 
if (!Game.paused && initialized) {	

	if (position_meeting(device_mouse_x(0), device_mouse_y(0), self)) {
		draw_set_font(fnt_HUD_Details);
		draw_set_color(c_white);
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
		draw_text(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0)-60, unit_data.name)
		var _health_pct = floor(round(100*hp/unit_data.max_hp)/10);
		draw_text(device_mouse_x_to_gui(0), device_mouse_y_to_gui(0)-40, "["+string_repeat("|",_health_pct)+string_repeat(" ",10-_health_pct)+"] "+string(hp)+"/"+string(unit_data.max_hp))
	}
	
}
