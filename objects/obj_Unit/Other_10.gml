/// @description Update based on the unit ID
unit_data = Game.units[? unit_id];
opposing_type = unit_data.type == "enemy" ? "ally" : "enemy";
hp = unit_data.max_hp;
nearest_opposing_unit_array = [noone, 999999999];



alarm[0] = unit_data.idle_animation_length;
alarm[1] = unit_data.move_animation_length;
alarm[2] = unit_data.attack_animation_length;


initialized = true;