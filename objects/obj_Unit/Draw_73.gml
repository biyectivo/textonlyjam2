/// @description Unit end draw event destroy if outside boundaries
if (!Game.paused && initialized) {
	if (x < -100 || x > room_width + 100 || y < -100 || y > room_height + 100) {
		instance_destroy();
	}
}