/// @description Spawn rate
var _coords = fnc_Coords(irandom_range(0,5),10);
var _id = instance_create_layer(_coords[0], _coords[1], layer_get_id("lyr_Instances"), obj_Unit);
with (_id) {
	unit_id = "Snake";
	event_perform(ev_other, ev_user0);
}

alarm[0] = spawn_rate;