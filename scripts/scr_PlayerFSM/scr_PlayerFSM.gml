
/// @function fnc_PlayerFSM_Idle
/// @description Idle state for player
function fnc_PlayerFSM_Idle() {

	// Get Input
	fnc_PlayerFSM_GetInput();
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
	
}


/// @function fnc_PlayerFSM_Move
/// @description Move state for player
function fnc_PlayerFSM_Move() {
	
	
	// Via keys - horizontal	
	var _commit_speed;
	var _normalizing_factor;	
	//_normalizing_factor = sqrt(power(player_horizontal_input*walk_speed,2)+power(player_vertical_input*walk_speed,2));
	
	// Normal move
	
	if (player_horizontal_input != 0) {
		var _collision_pos = (player_horizontal_input == 1 ? bbox_right : bbox_left);
		
		if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _collision_pos + floor(player_horizontal_input*walk_hspeed), y) != 1
			 && tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _collision_pos + floor(player_horizontal_input*walk_hspeed), bbox_top) != 1
			 && tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _collision_pos + floor(player_horizontal_input*walk_hspeed), bbox_bottom) != 1
		) {
			_commit_speed = floor(player_horizontal_input*walk_hspeed);
		}
		else {
			_commit_speed = 0;
			
		}
		
		// Check for collisions with collisionable objects and snap to the bounding box if needed
		if (place_meeting(x + _commit_speed, y, cls_Collisionable)) {
			var _id = instance_place(x + _commit_speed, y, cls_Collisionable);
			
			while (!place_meeting(x + sign(_commit_speed), y, cls_Collisionable)) {
				x = x + sign(_commit_speed);
			}
			
		}
		else {
			x = x + _commit_speed;
		}
		
	}
	
	// Via keys - vertical
	if (player_vertical_input != 0) {
		var _collision_pos = (player_vertical_input == 1 ? bbox_bottom : bbox_top);
		
		if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), x, _collision_pos + floor(player_vertical_input*walk_vspeed)) != 1
			&& tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), bbox_left, _collision_pos + floor(player_vertical_input*walk_vspeed)) != 1
			&& tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), bbox_right, _collision_pos + floor(player_vertical_input*walk_vspeed)) != 1
		) {
			_commit_speed = floor(player_vertical_input*walk_vspeed);
		}
		else {
			_commit_speed = 0;
		}
		
		// Check for collisions with collisionable objects and snap to the bounding box if needed
		if (place_meeting(x, y + _commit_speed, cls_Collisionable)) {			
			var _id = instance_place(x, y + _commit_speed, cls_Collisionable);						
			
			while (!place_meeting(x, y + sign(_commit_speed), cls_Collisionable)) {
				y = y + sign(_commit_speed);
			}	
			
		}
		else {
			y = y + _commit_speed;
		}	
		
	}
	
	// Update facing
	if (player_horizontal_input != 0) {
		facing = (player_horizontal_input == 1 ? FACING.EAST : FACING.WEST);
	}
	if (player_vertical_input != 0) {
		facing = (player_vertical_input  == 1 ? FACING.SOUTH : FACING.NORTH);
	}
	
	
	// Get Input
	fnc_PlayerFSM_GetInput();
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
	
}


function fnc_PlayerFSM_Die() {
	facing = FACING.EAST;
	if (obj_Player.alarm[3] == -1) {
		obj_Player.alarm[3] = (animation_lengths[? "Die"]-1) * animation_speeds[? "Die"];		
	}
	controllable = false;
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
}

function fnc_PlayerFSM_GetInput() {
	
	// Detect gamepad
	var _maxpads = gamepad_get_device_count();
	Game.primary_gamepad = -1;
	
	var _i=0; 
	while (_i < _maxpads && Game.primary_gamepad == -1) {
		Game.primary_gamepad = gamepad_is_connected(_i) ? _i : -1;
		_i++;
	}
	
	// Keyboard
	player_horizontal_input = keyboard_check(Game.controls[? "right"]) - keyboard_check(Game.controls[? "left"]);
	if (Game.primary_gamepad != -1) {
		player_horizontal_input = (gamepad_axis_value(Game.primary_gamepad, gp_axislh) > GAMEPAD_THRESHOLD || keyboard_check(Game.controls[? "right"])) - (gamepad_axis_value(Game.primary_gamepad, gp_axislh) < -GAMEPAD_THRESHOLD || keyboard_check(Game.controls[? "left"]));
	}
	
	player_vertical_input = keyboard_check(Game.controls[? "down"]) - keyboard_check(Game.controls[? "up"]);
	if (Game.primary_gamepad != -1) {
		player_vertical_input = (gamepad_axis_value(Game.primary_gamepad, gp_axislv) > GAMEPAD_THRESHOLD || keyboard_check(Game.controls[? "down"])) - (gamepad_axis_value(Game.primary_gamepad, gp_axislv) < -GAMEPAD_THRESHOLD || keyboard_check(Game.controls[? "up"]));
	}
	
}

function fnc_PlayerFSM_Animate(_reset_animation) {
	
	animation_frame_count = _reset_animation ? 0 : ((animation_frame_count + 1) % animation_lengths[? animation_name]);
	animation_frame = animation_frame_count * animation_spacings[? animation_name];
	
	/* Currently working in 4-directions if the sprite is a multi-frame sprite (imported from spritesheet) like so:
		
		[ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
			0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23
		|----------||----------------------||----------------------------------|
			idle             move                          attack
		    ^           ^                       ^
		startindex   startindex              startindex
			
		order of directions determined by FACING enum
		sprite should have centerpoint as origin
		
	*/
		
	if (animation_dirs >= 4) {
		image_index = animation_startindices[? animation_name] + facing * animation_lengths[? animation_name] + animation_frame;		
		if  (Game.debug) {
			show_debug_message(name+" @ "+state_name+" facing "+string(facing)+": ("+string(_reset_animation)+") "+string(animation_startindices[? animation_name])+"+"+string(facing)+"*"+string(animation_lengths[? animation_name])+"+"+string(animation_frame)+"="+string(image_index));
		}
	}
	else if (animation_dirs	== 2) {		
		image_xscale = (facing == FACING.EAST ? -base_xscale : base_xscale);			
		image_index = player_horizontal_input == -1 ? 1 : (player_horizontal_input == 1 ? 2 : 0);			
	}
	else {
		image_index = animation_startindices[? animation_name] + animation_frame;
	}

}

function fnc_PlayerFSM_ResetAnimation() {
	fnc_PlayerFSM_Animate(true);
	alarm[0] = animation_speeds[? animation_name];
}

function fnc_PlayerFSM_Transition() {
	last_state = state;
	last_state_name = state_name;
	
	// Transition matrix	
	if (state_name == "Idle") {		
		if (hp <= 0) {		
			state = fnc_PlayerFSM_Die;
			state_name = "Die";		
		}
		else if (controllable && (player_horizontal_input != 0 || player_vertical_input != 0)) {			
			state = fnc_PlayerFSM_Move;	
			state_name = "Move";
		}
		else {
			state = fnc_PlayerFSM_Idle;	
			state_name = "Idle";		
		}
	}
	else if (state_name == "Move") {
		if (hp <= 0) {		
			state = fnc_PlayerFSM_Die;
			state_name = "Die";		
		}
		else if (player_horizontal_input == 0 && player_vertical_input == 0) {
			state = fnc_PlayerFSM_Idle;
			state_name = "Idle";
		}
		else {		
			state = fnc_PlayerFSM_Move;	
			state_name = "Move";
		}
	}
	else { // Die
		
	}
	
	// Reset animation
	reset_animation = (state != last_state);
	animation_name = state_name;
	
	if (reset_animation) {
		if (Game.debug) {
			show_debug_message(name+" transitioning from "+string(last_state)+" "+last_state_name+" to "+string(state)+" "+state_name);
		}
		fnc_PlayerFSM_ResetAnimation();
	}
}