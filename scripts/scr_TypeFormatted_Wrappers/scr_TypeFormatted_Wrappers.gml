
function fnc_draw_text_ext_transformed_color(x,y,string,sep,w,xscale,yscale,angle,c1,c2,c3,c4,alpha) {
	var _c = draw_get_color();
	var _a = draw_get_alpha();
	if (sep == -1 && w == -1 && xscale == 1 && yscale == 1 && angle == 0 && c1==_c && c2==_c && c3==_c && c4==_c && abs(alpha-_a)<=TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE) {
		draw_text(x, y, string);
	}
	else if (sep == -1 && w == -1 && xscale == 1 && yscale == 1 && angle == 0) {
		draw_text_color(x, y, string, c1, c2, c3, c4, alpha);
	}
	else if (xscale == 1 && yscale == 1 && angle == 1 && c1==_c && c2==_c && c3==_c && c4==_c && abs(alpha-_a)<=TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE) {
		draw_text_ext(x,y,string,sep,w);
	}
	else if (xscale == 1 && yscale == 1 && angle == 1) {
		draw_text_ext_color(x,y,string,sep,w,c1,c2,c3,c4,alpha);
	}
	else if (sep == -1 && w == -1) {
		draw_text_transformed(x, y, string, xscale, yscale, angle);
	}
	else {
		draw_text_ext_transformed(x,y,string,sep,w,xscale,yscale,angle);
	}
}


function fnc_draw_text_ext_transformed(x,y,string,sep,w,xscale,yscale,angle) {
	if (sep == -1 && w == -1 && xscale == 1 && yscale == 1 && angle == 0) {
		draw_text(x, y, string);
	}
	else if (xscale == 1 && yscale ==1 && angle == 1) {
		draw_text_ext(x,y,string,sep,w);
	}
	else if (sep == -1 && w == -1) {
		draw_text_transformed(x, y, string, xscale, yscale, angle);
	}
	else {
		draw_text_ext_transformed(x,y,string,sep,w,xscale,yscale,angle);
	}
}

function fnc_draw_set_alpha(alpha) {
	if (abs(draw_get_alpha() - alpha) > TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE) {
		draw_set_alpha(alpha);	
	}
}

function fnc_draw_set_color(color_string) {	
	if (string_copy(color_string, 1, 1) == "$") { // Change color, custom
		var _col = make_color_rgb(	real(base_convert(string_copy(color_string, 6, 2), 16, 10)), 
									real(base_convert(string_copy(color_string, 4, 2), 16, 10)),
									real(base_convert(string_copy(color_string, 2, 2), 16, 10)));
		if (draw_get_color() != _col) {
			draw_set_color(_col);
		}
	}
	else if (string_copy(color_string, 1, 8) == "c_random") {		
		var _col = make_color_rgb(irandom_range(0,255), irandom_range(0,255), irandom_range(0,255));
		draw_set_color(_col);
	}
	else if (string_copy(color_string, 1, 2) == "c_") { // Change color, built-in constant
		if (!is_undefined(ds_map_find_value(global.tf_colors, color_string)) && draw_get_color() != ds_map_find_value(global.tf_colors, color_string)) {
			draw_set_color(global.tf_colors[? color_string]);
		}
	}
	
}

function fnc_draw_set_font(font) {	
	if (draw_get_font() != font) {		
		draw_set_font(font);
	}
}
