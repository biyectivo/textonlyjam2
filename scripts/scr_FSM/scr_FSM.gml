#region Snake

	#region State FSM
		function fnc_StateFSM_Snake_Idle() {
			fnc_StateFSM_Snake_Transition();
		}

		function fnc_StateFSM_Snake_Move() {
			x = x - unit_data.move_speed;
			fnc_StateFSM_Snake_Transition();
		}

		function fnc_StateFSM_Snake_Attack() {			
			if (alarm[3] == -1 && alarm[2] == -1) {
				alarm[3] = unit_data.attack_rate;
			}
			else if (alarm[2] == 10) {
				nearest_opposing_unit_array[0].hp = nearest_opposing_unit_array[0].hp - unit_data.attack_damage_hp;
				nearest_opposing_unit_array[0].alarm[4] = HIT_SHADER_EFFECT_DURATION;
			}
			fnc_StateFSM_Snake_Transition();
		}
		
		function fnc_StateFSM_Snake_Die() {
			instance_destroy();
		}

		function fnc_StateFSM_Snake_Transition() {
			nearest_opposing_unit_array = fnc_Nearest_Unit(opposing_type, opposing_type == "enemy" ? "right" : "left");
			if (hp <= 0) {
				state = "Die";	
			}
			else if (nearest_opposing_unit_array[1] <= unit_data.attack_range) {
				state = "Attack";
			}
			else {
				state = "Move";	
			}
		}
	#endregion
	
	#region Animation FSM
		function fnc_AnimateFSM_Snake_Idle() {			
			fnc_AnimateFSM_GenericEnemy_Idle();
		}
		
		function fnc_AnimateFSM_Snake_Move() {
			fnc_AnimateFSM_GenericEnemy_Idle();
		}

		function fnc_AnimateFSM_Snake_Attack() {			
			fnc_AnimateFSM_GenericEnemy_Action();
		}

		
		function fnc_AnimateFSM_Snake_Die() {
		}

	#endregion
#endregion

#region Shooter

	#region State FSM
		function fnc_StateFSM_Shooter_Idle() {
			fnc_StateFSM_Shooter_Transition();
		}

		function fnc_StateFSM_Shooter_Move() {			
		}

		function fnc_StateFSM_Shooter_Attack() {
			if (alarm[3] == -1 && alarm[2] == -1) {
				alarm[3] = unit_data.attack_rate;
			}
			else if (alarm[2] == 10) {
				var _id = instance_create_layer(x+10, y, layer_get_id("lyr_Projectiles"), obj_Projectile);
				with (_id) {
					projectile_id = other.unit_data.attack_projectile_id;
					creator_unit_id = other.id;
					projectile_angle = (other.opposing_type == "enemy" ? 0 : 180);
					event_perform(ev_other, ev_user0);
				}
			}
			fnc_StateFSM_Shooter_Transition();
		}
		
		function fnc_StateFSM_Shooter_Die() {
			instance_destroy();
		}


		function fnc_StateFSM_Shooter_Transition() {
			nearest_opposing_unit_array = fnc_Nearest_Unit(opposing_type, opposing_type == "enemy" ? "right" : "left");
			if (hp <= 0) {
				state = "Die";
			}
			else if (nearest_opposing_unit_array[1] <= unit_data.attack_range) {
				state = "Attack";
			}
			else {
				state = "Idle";	
			}
		}
	#endregion
	
	#region Animation FSM
		function fnc_AnimateFSM_Shooter_Idle() {			
			//if live_call() return live_result;
			draw_set_color(c_white);
			draw_set_halign(fa_center);
			draw_set_valign(fa_middle);
			draw_set_font(fnt_Monster_Bold);
	
			var _angle = 2*sin(alarm[0]/unit_data.idle_animation_length  * 2*PI);
	
			draw_text_transformed(x, y, "i", 0.4, 0.4, _angle);
			if (alarm[4] >= 0) {
				draw_set_color(c_red);
				draw_set_alpha(0.7);
				draw_text_transformed(x, y, "i", 0.4, 0.4, _angle);
				draw_set_alpha(1);
			}
			
		}
		
		function fnc_AnimateFSM_Shooter_Move() {
			fnc_AnimateFSM_Shooter_Idle();
		}

		function fnc_AnimateFSM_Shooter_Attack() {			
			fnc_AnimateFSM_Shooter_Idle();
		}

		function fnc_AnimateFSM_Shooter_Die() {			
		}
	#endregion
#endregion


#region Energizer

	#region State FSM
		function fnc_StateFSM_Energizer_Idle() {
			if (alarm[5] == -1) {
				alarm[5] = unit_data.energy_creation_rate;
			}
			fnc_StateFSM_Energizer_Transition();
		}

		function fnc_StateFSM_Energizer_CreateEnergy() {
			Game.current_energy = Game.current_energy + unit_data.energy_creation_num;
			fnc_StateFSM_Energizer_Transition();
		}
		
		function fnc_StateFSM_Energizer_Die() {
			instance_destroy();
		}


		function fnc_StateFSM_Energizer_Transition() {
			if (hp <= 0) {
				state = "Die";
			}
			else if (alarm[5] == 1) {
				state = "CreateEnergy";
			}
			else {
				state = "Idle";	
			}
		}
	#endregion
	
	#region Animation FSM
		function fnc_AnimateFSM_Energizer_Idle() {			
			//if live_call() return live_result;
			draw_set_color(c_aqua);
			draw_set_halign(fa_center);
			draw_set_valign(fa_middle);
			draw_set_font(fnt_Monster_Bold);
	
			var _angle = 3*sin(alarm[0]/unit_data.idle_animation_length  * 2*PI);
	
			draw_text_transformed(x, y, "E", 0.4, 0.4, _angle);
			
			if (alarm[4] >= 0) {
				draw_set_color(c_red);
				draw_set_alpha(0.7);
				draw_text_transformed(x, y, "E", 0.4, 0.4, _angle);
				draw_set_alpha(1);
			}
			
		}
		
		function fnc_AnimateFSM_Energizer_CreateEnergy() {
			show_debug_message("*");
		}
		
		function fnc_AnimateFSM_Energizer_Die() {			
		}
	#endregion
#endregion


#region Generic Enemy Draw Functions
	function fnc_AnimateFSM_GenericEnemy_Idle() {
		//if live_call() return live_result			
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_set_font(fnt_Monster_Bold);
	
		var _angle = 10*sin(alarm[0]/unit_data.idle_animation_length * 2*PI);
				
		// Base
		draw_set_color(fnc_TypeFormatted_Color(unit_data.base_color));
		draw_text_transformed(x, y, unit_data.base_letter, unit_data.base_xscale, abs(unit_data.base_xscale), _angle);
			
		// Shader hit
		if (alarm[4] >= 0) {
			draw_set_color(c_red);
			draw_set_alpha(0.7);
			draw_text_transformed(x, y, unit_data.base_letter, unit_data.base_xscale, abs(unit_data.base_xscale), _angle);
			draw_set_alpha(1);
		}
			
		// Adornment 1
		if (unit_data.base_adornment1 != "") {
			draw_set_color(fnc_TypeFormatted_Color(unit_data.base_adornment1_color));
	
			var _x = x+unit_data.base_adornment1_xoffset;
			var _y = y+unit_data.base_adornment1_yoffset-sin((1-alarm[0]/unit_data.idle_animation_length)*2*PI)*3;
		
			draw_text_transformed(_x, _y, unit_data.base_adornment1, unit_data.base_adornment1_xscale, abs(unit_data.base_adornment1_xscale), 0);
			
			// Shader hit
			if (alarm[4] >= 0) {
				draw_set_color(c_red);
				draw_set_alpha(0.7);
				draw_text_transformed(_x, _y, unit_data.base_adornment1, unit_data.base_adornment1_xscale, abs(unit_data.base_adornment1_xscale), 0);
				draw_set_alpha(1);
			}
		}
			
		// Adornment 2
		if (unit_data.base_adornment2 != "") {
			draw_set_color(fnc_TypeFormatted_Color(unit_data.base_adornment2_color));
	
			var _x = x+unit_data.base_adornment2_xoffset;
			var _y = y+unit_data.base_adornment2_yoffset-sin((1-alarm[0]/unit_data.idle_animation_length)*2*PI)*3;
		
			draw_text_transformed(_x, _y, unit_data.base_adornment2, unit_data.base_adornment2_xscale, abs(unit_data.base_adornment2_xscale), 0);
			
			// Shader hit
			if (alarm[4] >= 0) {
				draw_set_color(c_red);
				draw_set_alpha(0.7);
				draw_text_transformed(_x, _y, unit_data.base_adornment2, unit_data.base_adornment2_xscale, abs(unit_data.base_adornment2_xscale), 0);
				draw_set_alpha(1);
			}
		}
	}

	function fnc_AnimateFSM_GenericEnemy_Action() {
		//if live_call() return live_result			
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_set_font(fnt_Monster_Bold);
		
		if (alarm[3] != -1) {
			var _angle = 0;
		}
		else {				
			var _angle = 20*sin(alarm[2]/unit_data.attack_animation_length * 2*PI);
		}
		
		// Base
		draw_set_color(fnc_TypeFormatted_Color(unit_data.action_color));
		draw_text_transformed(x, y, unit_data.action_letter, unit_data.action_xscale, abs(unit_data.action_xscale), _angle);
			
		// Shader hit
		if (alarm[4] >= 0) {
			draw_set_color(c_red);
			draw_set_alpha(0.7);
			draw_text_transformed(x, y, unit_data.action_letter, unit_data.action_xscale, abs(unit_data.action_xscale), _angle);
			draw_set_alpha(1);
		}
			
		// Adornment 1
		if (unit_data.action_adornment1 != "") {
			draw_set_color(fnc_TypeFormatted_Color(unit_data.action_adornment1_color));
	
			var _x = x+unit_data.action_adornment1_xoffset;
			var _y = y+unit_data.action_adornment1_yoffset-sin((1-alarm[0]/unit_data.attack_animation_length)*2*PI)*3;
		
			draw_text_transformed(_x, _y, unit_data.action_adornment1, unit_data.action_adornment1_xscale, abs(unit_data.action_adornment1_xscale), _angle);
			
			// Shader hit
			if (alarm[4] >= 0) {
				draw_set_color(c_red);
				draw_set_alpha(0.7);
				draw_text_transformed(_x, _y, unit_data.action_adornment1, unit_data.action_adornment1_xscale, abs(unit_data.action_adornment1_xscale), _angle);
				draw_set_alpha(1);
			}
		}
			
		// Adornment 2
		if (unit_data.action_adornment2 != "") {
			draw_set_color(fnc_TypeFormatted_Color(unit_data.action_adornment2_color));
	
			var _x = x+unit_data.action_adornment2_xoffset;
			var _y = y+unit_data.action_adornment2_yoffset-sin((1-alarm[0]/unit_data.attack_animation_length)*2*PI)*3;
		
			draw_text_transformed(_x, _y, unit_data.action_adornment2, unit_data.action_adornment2_xscale, abs(unit_data.action_adornment2_xscale), _angle);
			
			// Shader hit
			if (alarm[4] >= 0) {
				draw_set_color(c_red);
				draw_set_alpha(0.7);
				draw_text_transformed(_x, _y, unit_data.action_adornment2, unit_data.action_adornment2_xscale, abs(unit_data.action_adornment2_xscale), _angle);
				draw_set_alpha(1);
			}
		}
	}

#endregion

#region Utility Functions	

	function fnc_Nearest_Unit(_faction, _distance_type) {
		
		//if live_call() return live_result;
		
		var _nearest = noone;
		var _dist = 999999999;
		var _n = instance_number(obj_Unit);		
		
		for (var _i=0; _i<_n; _i++) {
			var _otherid = instance_find(obj_Unit, _i);
			if (_otherid != id && _otherid.unit_data.type == _faction) {
				if (_distance_type == "horizontal") {
					var _distance = (y == _otherid.y) ? abs(x - _otherid.x) : 999999999;
				}
				else if (_distance_type == "left") {
					var _distance = (y == _otherid.y) ? (x - _otherid.x > 0 ? x - _otherid.x : 999999999) : 999999999;
				}
				else if (_distance_type == "right") {
					var _distance = (y == _otherid.y) ? (_otherid.x - x > 0 ? _otherid.x - x : 999999999) : 999999999;
				}
				else if (_distance_type == "vertical") {
					var _distance = abs(y - _otherid.y);
				}
				else if (_distance_type == "manhattan") {
					var _distance = abs(x - _otherid.x) + abs(y - _otherid.y);
				}
				else if (_distance_type == "max") {
					var _distance = max(abs(x - _otherid.x), abs(y - _otherid.y));
				}
				else {
					var _distance = point_distance(x, y, _otherid.x, _otherid.y);
				}			
			
				if (_distance < _dist) {
					_nearest = _otherid;
					_dist = _distance;
				}
			}
		}
		return [_nearest, _dist];
	}
	
#endregion