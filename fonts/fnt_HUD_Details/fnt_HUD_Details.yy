{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Consolas",
  "styleName": "Bold",
  "size": 10.0,
  "bold": true,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":7,"h":16,"character":32,"shift":7,"offset":0,},
    "33": {"x":97,"y":38,"w":3,"h":16,"character":33,"shift":7,"offset":2,},
    "34": {"x":90,"y":38,"w":5,"h":16,"character":34,"shift":7,"offset":1,},
    "35": {"x":81,"y":38,"w":7,"h":16,"character":35,"shift":7,"offset":0,},
    "36": {"x":72,"y":38,"w":7,"h":16,"character":36,"shift":7,"offset":0,},
    "37": {"x":62,"y":38,"w":8,"h":16,"character":37,"shift":7,"offset":0,},
    "38": {"x":52,"y":38,"w":8,"h":16,"character":38,"shift":7,"offset":0,},
    "39": {"x":47,"y":38,"w":3,"h":16,"character":39,"shift":7,"offset":2,},
    "40": {"x":40,"y":38,"w":5,"h":16,"character":40,"shift":7,"offset":1,},
    "41": {"x":33,"y":38,"w":5,"h":16,"character":41,"shift":7,"offset":1,},
    "42": {"x":102,"y":38,"w":7,"h":16,"character":42,"shift":7,"offset":0,},
    "43": {"x":24,"y":38,"w":7,"h":16,"character":43,"shift":7,"offset":0,},
    "44": {"x":9,"y":38,"w":4,"h":16,"character":44,"shift":7,"offset":1,},
    "45": {"x":2,"y":38,"w":5,"h":16,"character":45,"shift":7,"offset":1,},
    "46": {"x":245,"y":20,"w":3,"h":16,"character":46,"shift":7,"offset":2,},
    "47": {"x":236,"y":20,"w":7,"h":16,"character":47,"shift":7,"offset":0,},
    "48": {"x":227,"y":20,"w":7,"h":16,"character":48,"shift":7,"offset":0,},
    "49": {"x":218,"y":20,"w":7,"h":16,"character":49,"shift":7,"offset":0,},
    "50": {"x":209,"y":20,"w":7,"h":16,"character":50,"shift":7,"offset":0,},
    "51": {"x":200,"y":20,"w":7,"h":16,"character":51,"shift":7,"offset":0,},
    "52": {"x":191,"y":20,"w":7,"h":16,"character":52,"shift":7,"offset":0,},
    "53": {"x":15,"y":38,"w":7,"h":16,"character":53,"shift":7,"offset":0,},
    "54": {"x":120,"y":38,"w":7,"h":16,"character":54,"shift":7,"offset":0,},
    "55": {"x":214,"y":38,"w":7,"h":16,"character":55,"shift":7,"offset":0,},
    "56": {"x":129,"y":38,"w":7,"h":16,"character":56,"shift":7,"offset":0,},
    "57": {"x":46,"y":56,"w":7,"h":16,"character":57,"shift":7,"offset":0,},
    "58": {"x":41,"y":56,"w":3,"h":16,"character":58,"shift":7,"offset":2,},
    "59": {"x":35,"y":56,"w":4,"h":16,"character":59,"shift":7,"offset":1,},
    "60": {"x":27,"y":56,"w":6,"h":16,"character":60,"shift":7,"offset":0,},
    "61": {"x":18,"y":56,"w":7,"h":16,"character":61,"shift":7,"offset":0,},
    "62": {"x":10,"y":56,"w":6,"h":16,"character":62,"shift":7,"offset":1,},
    "63": {"x":2,"y":56,"w":6,"h":16,"character":63,"shift":7,"offset":1,},
    "64": {"x":242,"y":38,"w":8,"h":16,"character":64,"shift":7,"offset":0,},
    "65": {"x":232,"y":38,"w":8,"h":16,"character":65,"shift":7,"offset":0,},
    "66": {"x":55,"y":56,"w":7,"h":16,"character":66,"shift":7,"offset":0,},
    "67": {"x":223,"y":38,"w":7,"h":16,"character":67,"shift":7,"offset":0,},
    "68": {"x":205,"y":38,"w":7,"h":16,"character":68,"shift":7,"offset":0,},
    "69": {"x":197,"y":38,"w":6,"h":16,"character":69,"shift":7,"offset":1,},
    "70": {"x":189,"y":38,"w":6,"h":16,"character":70,"shift":7,"offset":1,},
    "71": {"x":180,"y":38,"w":7,"h":16,"character":71,"shift":7,"offset":0,},
    "72": {"x":171,"y":38,"w":7,"h":16,"character":72,"shift":7,"offset":0,},
    "73": {"x":162,"y":38,"w":7,"h":16,"character":73,"shift":7,"offset":0,},
    "74": {"x":155,"y":38,"w":5,"h":16,"character":74,"shift":7,"offset":1,},
    "75": {"x":146,"y":38,"w":7,"h":16,"character":75,"shift":7,"offset":0,},
    "76": {"x":138,"y":38,"w":6,"h":16,"character":76,"shift":7,"offset":1,},
    "77": {"x":182,"y":20,"w":7,"h":16,"character":77,"shift":7,"offset":0,},
    "78": {"x":111,"y":38,"w":7,"h":16,"character":78,"shift":7,"offset":0,},
    "79": {"x":173,"y":20,"w":7,"h":16,"character":79,"shift":7,"offset":0,},
    "80": {"x":198,"y":2,"w":7,"h":16,"character":80,"shift":7,"offset":0,},
    "81": {"x":179,"y":2,"w":8,"h":16,"character":81,"shift":7,"offset":0,},
    "82": {"x":170,"y":2,"w":7,"h":16,"character":82,"shift":7,"offset":0,},
    "83": {"x":161,"y":2,"w":7,"h":16,"character":83,"shift":7,"offset":0,},
    "84": {"x":152,"y":2,"w":7,"h":16,"character":84,"shift":7,"offset":0,},
    "85": {"x":143,"y":2,"w":7,"h":16,"character":85,"shift":7,"offset":0,},
    "86": {"x":133,"y":2,"w":8,"h":16,"character":86,"shift":7,"offset":0,},
    "87": {"x":124,"y":2,"w":7,"h":16,"character":87,"shift":7,"offset":0,},
    "88": {"x":114,"y":2,"w":8,"h":16,"character":88,"shift":7,"offset":0,},
    "89": {"x":104,"y":2,"w":8,"h":16,"character":89,"shift":7,"offset":0,},
    "90": {"x":189,"y":2,"w":7,"h":16,"character":90,"shift":7,"offset":0,},
    "91": {"x":97,"y":2,"w":5,"h":16,"character":91,"shift":7,"offset":1,},
    "92": {"x":79,"y":2,"w":7,"h":16,"character":92,"shift":7,"offset":0,},
    "93": {"x":73,"y":2,"w":4,"h":16,"character":93,"shift":7,"offset":1,},
    "94": {"x":64,"y":2,"w":7,"h":16,"character":94,"shift":7,"offset":0,},
    "95": {"x":54,"y":2,"w":8,"h":16,"character":95,"shift":7,"offset":0,},
    "96": {"x":47,"y":2,"w":5,"h":16,"character":96,"shift":7,"offset":0,},
    "97": {"x":38,"y":2,"w":7,"h":16,"character":97,"shift":7,"offset":0,},
    "98": {"x":29,"y":2,"w":7,"h":16,"character":98,"shift":7,"offset":0,},
    "99": {"x":20,"y":2,"w":7,"h":16,"character":99,"shift":7,"offset":0,},
    "100": {"x":11,"y":2,"w":7,"h":16,"character":100,"shift":7,"offset":0,},
    "101": {"x":88,"y":2,"w":7,"h":16,"character":101,"shift":7,"offset":0,},
    "102": {"x":207,"y":2,"w":7,"h":16,"character":102,"shift":7,"offset":0,},
    "103": {"x":59,"y":20,"w":7,"h":16,"character":103,"shift":7,"offset":0,},
    "104": {"x":216,"y":2,"w":7,"h":16,"character":104,"shift":7,"offset":0,},
    "105": {"x":148,"y":20,"w":7,"h":16,"character":105,"shift":7,"offset":0,},
    "106": {"x":140,"y":20,"w":6,"h":16,"character":106,"shift":7,"offset":0,},
    "107": {"x":131,"y":20,"w":7,"h":16,"character":107,"shift":7,"offset":0,},
    "108": {"x":122,"y":20,"w":7,"h":16,"character":108,"shift":7,"offset":0,},
    "109": {"x":113,"y":20,"w":7,"h":16,"character":109,"shift":7,"offset":0,},
    "110": {"x":104,"y":20,"w":7,"h":16,"character":110,"shift":7,"offset":0,},
    "111": {"x":95,"y":20,"w":7,"h":16,"character":111,"shift":7,"offset":0,},
    "112": {"x":86,"y":20,"w":7,"h":16,"character":112,"shift":7,"offset":0,},
    "113": {"x":77,"y":20,"w":7,"h":16,"character":113,"shift":7,"offset":0,},
    "114": {"x":157,"y":20,"w":6,"h":16,"character":114,"shift":7,"offset":1,},
    "115": {"x":68,"y":20,"w":7,"h":16,"character":115,"shift":7,"offset":0,},
    "116": {"x":50,"y":20,"w":7,"h":16,"character":116,"shift":7,"offset":0,},
    "117": {"x":41,"y":20,"w":7,"h":16,"character":117,"shift":7,"offset":0,},
    "118": {"x":31,"y":20,"w":8,"h":16,"character":118,"shift":7,"offset":0,},
    "119": {"x":21,"y":20,"w":8,"h":16,"character":119,"shift":7,"offset":0,},
    "120": {"x":12,"y":20,"w":7,"h":16,"character":120,"shift":7,"offset":0,},
    "121": {"x":2,"y":20,"w":8,"h":16,"character":121,"shift":7,"offset":0,},
    "122": {"x":238,"y":2,"w":7,"h":16,"character":122,"shift":7,"offset":0,},
    "123": {"x":230,"y":2,"w":6,"h":16,"character":123,"shift":7,"offset":0,},
    "124": {"x":225,"y":2,"w":3,"h":16,"character":124,"shift":7,"offset":2,},
    "125": {"x":165,"y":20,"w":6,"h":16,"character":125,"shift":7,"offset":1,},
    "126": {"x":64,"y":56,"w":7,"h":16,"character":126,"shift":7,"offset":0,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fnt_HUD_Details",
  "tags": [],
  "resourceType": "GMFont",
}